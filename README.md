**Objective**

Recursively crawl popular blogging website https://medium.com using Node.js and <br /> harvest all
possible hyperlinks that belong to medium.com and store them in db

---

Logic:  

    1. Keep a pendingUrlsTobeScraped array and keep adding urls fetched from scraped pages into it.   
    2. Used Redis cache/db to store already scrapedUrls and kept adding scrappedUrl into it.   
    3. These scrapedUrls are used to filter out already scraped urls from pendingUrlsTobeScraped array.   
    4. We need throttling of 5 req/sec so we make sure each request takes 200ms. i.e if req takes 50ms we add a delay of 150ms before making next request.    
    5. Store Url data in db in following way:      
        Assume scraper parses 4 URLs:     
        - https://medium.com/some/thing.     
        - https://medium.com/some/thing?param1=abc    
        - https://medium/com/some/thing?param2=xyz    
        - https://medium/com/some/thing?param1=def&param3=xxx    
        Database will contain the URL https://medium.com/some/thing with the a    
        reference count of 4. A unique list of parameters containing param1,param2,param3.    
    
Url Doc format stored in db:    
```javascript
{
    "_id" : ObjectId("5dbd8d452e34896cde22fab0"),
    "parameters" : [ 
        "redirect", 
        "source", 
        "operation"
    ],
    "url" : "https://medium.com/m/signin",
    "refCount" : 1566
}
```

---

## Project Setup

**Clone the project** : git clone https://sanket95@bitbucket.org/sanket95/medium-scraper.git    
**Command to install Redis Server** :    
brew install redis    
brew services start redis    
**Run command inside `medium-scraper` folder** :    
1. npm install    
2. node app/app.js    

Scraper will start scraping medium urls & storing them in mongoDb    

---
