let lastRequestTimeStore = 0;
class Throttle {

    constructor(minTimeBetweenRequestsInMs) {

        this.minTimeBetweenRequestsInMs = minTimeBetweenRequestsInMs;
    }
    /**
     *  This is a helper method that sleeps for the required amount before returning.
     * @param  {} callback method
     */
    async throttle(cb) {
        const nextRequestTime = this.getNextRequestTime();

        const timeToWait = (nextRequestTime - Date.now());
        console.log('TimeToWait in secs: ', timeToWait);
        if (timeToWait > 0) {
            await this.delay(timeToWait);
        }
        return cb();
    }

    delay(timeToWait) {
        return new Promise((resolve) => setTimeout(resolve(), timeToWait));
    }

    /** 
     * Based on set value of minTimeBetweenRequestsInMs which decides timeElapsed since lastRequest, 
     * we return currentTime or currTime + minTimeBetweenRequestsInMs     
     */
    getNextRequestTime() {
        const lastRequestTime = lastRequestTimeStore || 0;
        const timeElapsed = (Date.now() - lastRequestTime);

        if (timeElapsed < this.minTimeBetweenRequestsInMs) {
            // This request claims the next available time
            lastRequestTimeStore = lastRequestTime + this.minTimeBetweenRequestsInMs;
        } else {
            lastRequestTimeStore = Date.now();
        }

        return lastRequestTimeStore;
    }

}


module.exports.Throttle = Throttle;
