const rp = require('request-promise');
const URL = require('url');
class Http {
    constructor() {
    }

    /**
     * Get the Json from the given Url
     *
     * @template T
     * @param {string} url
     * @param {Headers} [headers={}]
     * @returns {Promise}
     *
     */
    async getJson(url, headers = {}, options) {
        return this.callJSONAPI(url, "GET", headers, undefined, options);
    }

    /**
     * Call the given API with given jsondata using the given method
     *
     * @template T
     * @param {string} url
     * @param {*} [jsonData]
     * @param {*} [headers]
     * @returns Promise
     *
     */
    async callJSONAPI(url, method, headers = {}, jsonData, options) {
        const requestOptions = {
            method: method,
            uri: url,
            headers: headers,
            json: options.json,
            resolveWithFullResponse: true
        };

        if (jsonData !== undefined) {
            requestOptions.body = jsonData;
        }

        return this.call(requestOptions);
    }

    /**
     * Generic request, req and res will be based on options
     *
     * @template T
     * @param  options
     * @returns {Promise}
     *
     */
    async call(options) {
        try {
            options = options || {};
            if (options.resolveWithFullResponse === undefined) {
                options.resolveWithFullResponse = true;
            }

            const response = await rp(options);

            let responseBody = response;
            if (options.resolveWithFullResponse) {
                responseBody = response.body;
            }
            return responseBody;
        } catch (err) {
            throw err;
        }
    }

    /**
     * get parsed url object and hence validate a url
     */
    getParsedUrlObj(url) {
        return URL.parse(url, true);
    }

}

module.exports.Http = Http;
