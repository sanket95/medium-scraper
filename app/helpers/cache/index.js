const Redis = require("ioredis");
const redis = new Redis();
const { CacheService } = require("./cache-service");

function getCacheService() {
    return new CacheService(redis); //can pass any other caching db like aerospike or localcaching like node-cache.
}

module.exports.getCacheService = getCacheService;
