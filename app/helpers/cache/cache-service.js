
class CacheService {

    constructor(client) {
        this.cache = client;
    }

    getItem(key) {
        return this.cache.get(key);
    }

    setItem(key, data) {
        return this.cache.set(key, data);
    }

}

module.exports.CacheService = CacheService;
