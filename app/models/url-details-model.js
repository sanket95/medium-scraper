const mongoose = require('mongoose');
const urlDetailsSchema = new mongoose.Schema({
    url: {
        type: String,
        unique: true,
        required: true,
    },
    refCount: {
        type: Number,
        required: true,
    },
    parameters: {
        type: Array,
        default: [],
        required: true,
    }
});
module.exports = mongoose.model('UrlDetails', urlDetailsSchema);
