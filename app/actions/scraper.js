const cheerio = require('cheerio');
const UrlDetails = require('../models/url-details-model');
const { Throttle } = require('../../throttle/throttle-requests');
const { Http } = require('../helpers/http/http');
const { DOMAIN_NAME, QPS } = require("../config");
const { getCacheService } = require("../helpers/cache");

class Scrapper {

    constructor() {
        this.httpHelper = new Http();
        this.cacheService = getCacheService();
        this.totalPendingUrlsToScrap = ['https://medium.com/'];
    }

    /**
     * Start scraping Urls and further generate more urls to scrap
     */
    async processUrlScraping() {
        while (this.totalPendingUrlsToScrap.length) {

            const scrapedUrl = this.totalPendingUrlsToScrap.shift();
            await this.cacheService.setItem(scrapedUrl, true);
            //add scrapedUrl to scrapedUrls map
            await this.updateOrCreateUrlDoc(scrapedUrl);
            const minTimeBetweenRequestsInMs = 1000 / QPS;
            await new Throttle(minTimeBetweenRequestsInMs).throttle(async () => {
                this.totalPendingUrlsToScrap = await this.fetchData(scrapedUrl)
            });
        }
    }

    /**
     * scrap url data and generate all links with 'medium.com' domain
     * @param  {} scrapedUrl
     */
    async fetchData(scrapedUrl) {
        let newScrapingUrls = [];
        try {
            const result = await this.httpHelper.getJson(scrapedUrl, {}, { json: false });
            const $ = cheerio.load(result);
            $('a').filter((idx, data) => {
                if (this.httpHelper.getParsedUrlObj(data.attribs.href) && this.isValidDomain(data.attribs.href)) {
                    newScrapingUrls.push(data.attribs.href);
                }
            });
        } catch (err) {
            console.log('url: ', scrapedUrl, ' - error occured while fetching url');
            return this.totalPendingUrlsToScrap;
        }

        //remove scrappedUrl from array incase it is again present in currScrapingUrls array
        return this.removeDuplicatesAndAlreadyScrapedUrls(newScrapingUrls);
    }

    /**
     * removes duplicates and filter out already scraped urls from pendingURLsTobeScraped
     * @param  {} newScrapingUrls
     */
    async removeDuplicatesAndAlreadyScrapedUrls(newScrapingUrls) {
        //making sure logic is in O(n) complexity
        const uniqueNewScrapingUrls = new Set(newScrapingUrls);
        //keep adding newScraping links into totalUrlToSrap array 
        const uniqueTotalUrlToScrap = new Set([...uniqueNewScrapingUrls, ...this.totalPendingUrlsToScrap]);
        this.totalPendingUrlsToScrap = Array.from(uniqueTotalUrlToScrap);
        //set already scraped link to false
        this.totalPendingUrlsToScrap = await Promise.all(this.totalPendingUrlsToScrap.map(async url => {
            if (await this.cacheService.getItem(url)) {
                return false
            }
            return url;
        }));
        return this.totalPendingUrlsToScrap.filter(x => x)//filter out already scraped links [value=false]
    }

    /**
     * Update url doc if url string is already present in db.
     * Otherwise create a new doc.
     * @param  {} url
     */
    async updateOrCreateUrlDoc(url) {
        let parsedUrlObj = this.httpHelper.getParsedUrlObj(url)
        let urlWithoutParams = url;
        if (url.includes('?')) {
            urlWithoutParams = url.substring(0, url.indexOf('?')); //remove query params
        }
        const res = await UrlDetails.findOne({ url: urlWithoutParams }).lean();
        let parameters = [...Object.keys(parsedUrlObj.query)];
        let urlObjectDB = {
            url: urlWithoutParams,
            refCount: 1,
            parameters: parameters
        };
        if (res) {
            let uniqueParameters = new Set([...parameters, ...res.parameters]);
            uniqueParameters = Array.from(uniqueParameters);
            urlObjectDB.parameters = uniqueParameters;
            urlObjectDB.refCount = res.refCount + 1;
            return UrlDetails.update({ _id: res._id.toString() }, urlObjectDB);
        }
        return UrlDetails.create(urlObjectDB);
    }

    /**
    * check for links that belong to specific hostName: [in our case medium.com] 
    * @param  {} url
    */
    isValidDomain(url) {
        let parsedUrlObj = this.httpHelper.getParsedUrlObj(url);
        if (parsedUrlObj.hostname === DOMAIN_NAME) {
            return true;
        }
        return false;
    }

}

module.exports.Scrapper = Scrapper;
