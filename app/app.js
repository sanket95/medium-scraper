var http = require('http');
const mongoose = require('mongoose');
const { Scrapper } = require('./actions/scraper');
//create a server object:
http.createServer(function (req, res) {
    res.write('Hello World!');
    res.end();
}).listen(3000, function () {
    console.log("server start at port 3000"); //the server object listens on port 3000
    return new Scrapper().processUrlScraping()
});

mongoose.connect('mongodb://localhost:27017/scraper', { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.once('open', () => {
    console.log(`MongoDb connected successfully, date is = ${new Date()}`);
});
